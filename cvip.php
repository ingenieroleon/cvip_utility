<?php
/*
Plugin Name: cvip
Description: Utilidades CVIP
Version: 4.20171124
Author: Ingeniero Leon
Author URI: http://ingenieroleon.com
*/



function add_cvip_widgets_collection_extra_site_origin($folders){
    $folders[] = __DIR__."/widgets-extra-bundle-site-origin/";
    return $folders;
}
add_filter('siteorigin_widgets_widget_folders', 'add_cvip_widgets_collection_extra_site_origin');



function cvip_prefixes( $class_prefixes ) {
	$class_prefixes[] = 'CVIP_';
	return $class_prefixes;
}
add_filter( 'siteorigin_widgets_field_class_prefixes', 'cvip_prefixes' );

?>
