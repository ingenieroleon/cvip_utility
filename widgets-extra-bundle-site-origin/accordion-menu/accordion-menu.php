<?php

/*
  Widget Name: CVIP Accordion menu
  Description: Menu de post vertical que se convierte en un accordeon en movil.
  Author: CVIP
  Author URI:
 */

if (!class_exists("SiteOrigin_Widget"))
    return;

class CVIP_Accordion_Menu extends SiteOrigin_Widget {

    function __construct() {
        $info = array('description' => __('Menu de post vertical que se convierte en un accordeon en movil.'));

        $option_query = array(
            'type' => 'posts',
            'label' => __('Especificar consulta de Posts'),
        );

        parent::__construct('cvip-accordion-menu', __('CVIP Accordion Menu'), $info, array(), array("some_posts" => $option_query), plugin_dir_path(__FILE__));
    }

    function get_template_name($instance) {
        return "widget";
    }

    function get_template_dir($instance) {
        return "views";
    }

    function get_style_name($instance) {
        return false;
    }

}

if (!function_exists("siteorigin_widget_register"))
    return;

siteorigin_widget_register('cvip-accordion-menu', __FILE__, 'CVIP_Accordion_Menu');
