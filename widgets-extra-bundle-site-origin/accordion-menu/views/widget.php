<?php
$post_selector_pseudo_query = $instance['some_posts'];

// //AGREGA VARIABLE QUE ORDENA POR CATEGORIA
// $post_selector_pseudo_query .= "&category__in=array(idCat)";

// Process the post selector pseudo query.
$processed_query = siteorigin_widget_post_selector_process_query($post_selector_pseudo_query);
// echo "<pre>";echo print_r($processed_query); echo "</pre>";
// echo "<pre>";echo $processed_query[tax_query][0][terms];echo "</pre>";
// Use the processed post selector query to find posts.
$parent_category = get_category_by_slug( $processed_query[tax_query][0][terms] );
$categories=get_terms('category','child_of='.$parent_category->term_id);

// echo "<pre>";echo print_r($categories); echo "</pre>";
$query_result = new WP_Query($processed_query);
//echo "<pre>Last SQL-Query: {$query_result->request}</pre>";
 ?>

 <div class="columnaproductos">
   <h3 class="dividermenucard">Menucard</h3>
    <ul class="nav nav-pills nav-stacked">
   <?php
  foreach ($categories as $category => $catdata) {  ?>

       <li class="amopciones"><a data-toggle="pill" href="#amp<?php echo $catdata->term_id ?>"><?php echo $catdata->name ?></a></li>
       <div id="amp<?php echo $catdata->term_id ?>" class="tituloproducto tab-pane fade">
         <h3><?php echo $catdata->name ?></h3>

         <?php $query_result = new WP_Query(array( 'cat' => $catdata->term_id )); ?>
           <?php while ($query_result->have_posts()) : $query_result->the_post(); ?>
             <div class="producto" data-imagen="<?php the_post_thumbnail_url( 'full' ); ?>">
                <div class="nombreproducto"><h5><?php the_title(); ?></h5></div>
                <!-- <div class="precio"><p>$6.50</p></div> -->
                <div class="describeproducto">
                  <p><?php echo strip_tags(substr(get_the_content(), 0, 250)) . ""; ?></p> 
                </div>
             </div>
          <?php endwhile; ?>

        </div><!-- CIERRE CATEGORIA <?php echo $catdata->name; wp_reset_postdata();  ?>-->

  <?php }//END FOREACH ?>
  </ul>
</div>
<div class="modal fade" id="modalproductos" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" tabindex="-1">
    <img id="imagenproductos" src="#" alt="" />
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">&times;</button>
</div>
<script type="text/javascript">
  jQuery('.producto').on('click', function(e) {
   e.preventDefault();
   jQuery("#imagenproductos").attr("src", jQuery(this).attr( 'data-imagen' ));
   jQuery('#modalproductos').modal('show');
 });
</script>
