<?php

/*
  Widget Name: CVIP Schelude
  Description: Widget para mostrar un horario en pantalla
  Author: Ingeniero Leon
  Author URI: http://ingenieroleon.com
 */

if (!class_exists("SiteOrigin_Widget"))
    return;

class CVIP_ScheduleWidget extends SiteOrigin_Widget {
    function __construct() {
        Require_once('posttypehorario.php');
        $info = array('description' => __('Widget para mostrar un horario en pantalla'));

        $args = array(
            'order' => 'DESC',
            'post_type' => "schedule",
            'post_status' => 'publish'
        );
        $schedules = get_posts($args);

        $options = array();

        foreach ($schedules as $item) {
            $options[$item->ID] = $item->post_title;
        }

        $option_radio = array(
            'type' => 'radio',
            'label' => __('Seleccionar Horario'),
            'default' => 'that_thing',
            'options' => $options);

        parent::__construct('cvip-schedule', __('CVIP Schelude'), $info, array(), array("radio_selections" => $option_radio), plugin_dir_path(__FILE__));
    }

    function get_template_name($instance) {
        return "widget";
    }

    function get_template_dir($instance) {
        return "views";
    }

    function get_style_name($instance) {
        return false;
    }

}

if (!function_exists("siteorigin_widget_register"))
    return;

siteorigin_widget_register('cvip-schedule', __FILE__, 'CVIP_ScheduleWidget');
