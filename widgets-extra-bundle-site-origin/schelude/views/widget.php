<?php
$schedule_id = $instance['radio_selections'];

$data = get_post_custom($schedule_id);
?>

<div class="bordehorario">
    <div class="dias">
        <span class="day-1" data-day="1">Lun</span>
        <span class="day-2" data-day="2">Mar</span>
        <span class="day-3" data-day="3">Mier</span>
        <span class="day-4" data-day="4">Jue</span>
        <span class="day-5" data-day="5">Vier</span>
        <span class="day-6" data-day="6">Sab</span>
        <span class="day-7" data-day="7">Dom</span>
    </div>

    <div class="numeros">
        <?php
        $w = date("w", mktime(0, 0, 0, date("n"), date("j"), date("Y")));
        $w = ($w == 0) ? 7 : $w;
        for ($i = 1; $i <= 7; $i++):
            $d = date("j", mktime(0, 0, 0, date("n"), date("j") - $w + $i, date("Y")));
            ?>
            <span class="day-<?php echo $i; ?>" data-day="<?php echo $i; ?>"><?php echo $d; ?></span>
        <?php endfor; ?>
    </div>

    <?php
    for ($day = 1; $day <= 7; $day++):
        ?>
        <div id="cvip-schedule-day-<?php echo $day; ?>" class="content-events-schedule" style="display: none;">
            <?php
            foreach ($data as $index => $value):
                if (strpos($index, "sche-title-$day-") === false)
                    continue;
                $n = str_replace("sche-title-$day-", "", $index);
                ?>
                <div class="contenidohorario">
                    <span class="hora"><?php echo $data["sche-hour-" . $day . "-" . $n][0] ?>:<?php echo (intval($data["sche-minutes-" . $day . "-" . $n][0]) < 10) ? "0" . $data["sche-minutes-" . $day . "-" . $n][0] : $data["sche-minutes-" . $day . "-" . $n][0]; ?></span>
                    <h3><?php echo $data["sche-title-" . $day . "-" . $n][0] ?></h3>
                    <span class="anfitrion"><?php echo $data["sche-info-" . $day . "-" . $n][0] ?></span>
                </div>
                <?php
            endforeach;
            ?>
        </div>
        <?php
    endfor;
    ?>

</div>


<script>
    jQuery(document).ready(function () {

        var date = new Date();
        var day = (date.getDay() == 0) ? 7 : date.getDay();

        jQuery("span.day-" + day).addClass("activo");
        jQuery("#cvip-schedule-day-" + day).show();

        jQuery(".dias span").click(function () {
            var day = jQuery(this).attr("data-day");
            jQuery(".content-events-schedule").hide();
            jQuery("#cvip-schedule-day-" + day).show();
            jQuery(".dias span").removeClass("activo");
            jQuery(".numeros span").removeClass("activo");
            jQuery(".day-" + day).addClass("activo");

        });
    });

</script>


