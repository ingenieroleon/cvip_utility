<?php

/** CODIGO GENERADOR DE POST TYPE
 * ***********************************************************
 */
// Register Custom Post Type
function cvip_create_post_type_schedule() {

    $labels = array(
        'name' => _x('Horarios', 'Post Type General Name', 'text_domain'),
        'singular_name' => _x('Horario', 'Post Type Singular Name', 'text_domain'),
        'menu_name' => __('Horarios', 'text_domain'),
        'name_admin_bar' => __('Horarios', 'text_domain'),
        'archives' => __('Item Archives', 'text_domain'),
        'parent_item_colon' => __('Item Padre:', 'text_domain'),
        'all_items' => __('Todos los Items', 'text_domain'),
        'add_new_item' => __('Agregar nuevo Item', 'text_domain'),
        'add_new' => __('Agregar nuevo', 'text_domain'),
        'new_item' => __('Nuevo Item', 'text_domain'),
        'edit_item' => __('Editar Item', 'text_domain'),
        'update_item' => __('Actualizar Item', 'text_domain'),
        'view_item' => __('Ver Item', 'text_domain'),
        'search_items' => __('Buscar...', 'text_domain'),
        'not_found' => __('No encontrado', 'text_domain'),
        'not_found_in_trash' => __('Not found in Trash', 'text_domain'),
        'featured_image' => __('Imagen Principal', 'text_domain'),
        'set_featured_image' => __('Establecer Imagen Principal', 'text_domain'),
        'remove_featured_image' => __('Eliminar Imagen Principal', 'text_domain'),
        'use_featured_image' => __('Usar como Imagen Principal', 'text_domain'),
        'insert_into_item' => __('Insertar', 'text_domain'),
        'uploaded_to_this_item' => __('Uploaded to this item', 'text_domain'),
        'items_list' => __('Items list', 'text_domain'),
        'items_list_navigation' => __('Items list navigation', 'text_domain'),
        'filter_items_list' => __('Filter items list', 'text_domain'),
    );
    $args = array(
        'label' => __('Horario', 'text_domain'),
        'description' => __('Tipo de dato para crear horarios', 'text_domain'),
        'labels' => $labels,
        'supports' => array('title', 'custom-fields',),
        'hierarchical' => false,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 5,
        'show_in_admin_bar' => true,
        'show_in_nav_menus' => true,
        'can_export' => true,
        'has_archive' => false,
        'exclude_from_search' => true,
        'publicly_queryable' => true,
        'capability_type' => 'page',
    );
    register_post_type('schedule', $args);
}

//META BOX
//ACTIVO EL POSTYPE AL ACTIVAR EL WIDGET
add_action('siteorigin_widgets_initialize_widget_cvip-schedule', 'cvip_create_post_type_schedule', 0);

add_action('add_meta_boxes', 'cvip_config_schedule_metabox');

function cvip_config_schedule_metabox() {
    add_meta_box('cvip-config-schedule-metabox', 'Configurar Horario', 'cvip_config_schedule_metabox_form', 'schedule', 'normal', 'high');
}

function cvip_config_schedule_metabox_form($post) {


    $data = get_post_custom($post->ID);
    ?>

    <div id="hour-cvip" style="display: none;">
        <div class="content-time-cvip">
            <label style="width: 100%;display: block;text-align: center;"> Hora </label>
            <select name="sche-hour">
                <?php
                for ($i = 0; $i <= 24; $i++):
                    ?>

                    <option value="<?php echo $i ?>"><?php echo ($i == 0) ? "00" : $i; ?></option>

                <?php endfor; ?>
            </select>
            :
            <select name="sche-minutes">
                <?php
                for ($i = 0; $i < 60; $i+=5):
                    ?>

                    <option value="<?php echo $i ?>"><?php echo ($i == 0) ? "00" : $i; ?></option>

                <?php endfor; ?>
            </select>
        </div>
    </div>

    <table>
        <tr><th>LUNES</th><th>MARTES</th><th>MIERCOLES</th><th>JUEVES</th><th>VIERNES</th><th>SABADO</th><th>DOMINGO</th></tr>
        <tr>
            <?php
            $html = "";
            $n = 0;
            for ($i = 1; $i <= 7; $i++) {
                $html = '<td>';
                foreach ($data as $index => $value) {

                    if (strpos($index, "sche-title-$i-") === false)
                        continue;

                    $n = str_replace("sche-title-$i-", "", $index);

                    $html.='<div class="event-content event-content-' . $i . '" id="event-content-' . $i . '-' . $n . '"><span class="btn-remove-event" data-id="' . $n . '" data-day="' . $i . '">X</span><div class="content-time-cvip"><label style="width: 100%;display: block;text-align: center;"> Hora </label>'
                            . '<select name="sche-hour-' . $i . '-' . $n . '">';
                    for ($f = 0; $f <= 24; $f++) {
                        $html .= '<option value="' . $f . '" ' . (($data["sche-hour-" . $i . "-" . $n][0] == $f) ? "selected" : "") . '>' . (($f == 0) ? "00" : $f) . '</option>';
                    }
                    $html.= '</select>:'
                            . '<select name="sche-minutes-' . $i . '-' . $n . '">';
                    for ($f = 0; $f < 60; $f+=5) {
                        $html .= '<option value="' . $f . '" ' . (($data["sche-minutes-" . $i . "-" . $n][0] == $f) ? "selected" : "") . '>' . (($f == 0) ? "00" : $f) . '</option>';
                    }
                    $html.= '</select>'
                            . '</div>'
                            . '<input type="text" name="sche-title-' . $i . '-' . $n . '" placeholder="Titulo" value="' . $data['sche-title-' . $i . '-' . $n][0] . '">'
                            . '<input type="text" name="sche-info-' . $i . '-' . $n . '" placeholder="Información adicional" value="' . $data['sche-info-' . $i . '-' . $n][0] . '">'
                            . '</div>';
                    $n++;
                }

                echo $html.='<button type="button" data-id="' . $n . '" data-day="' . $i . '" class="button button-primary button-large cvip-schedule-add-event">Agregar evento</button></td>';
            }
            ?>
            <td></td>

        </tr>
    </table>


    <?php
}

add_action('admin_head', 'cvip_config_schedule_style');
add_action('admin_head', 'cvip_config_schedule_script');

function cvip_config_schedule_style() {
    ?>
    <style>

        #cvip-config-schedule-metabox table th{
            width:14.2%;
        }

        #cvip-config-schedule-metabox table{
            width: 100%;
            margin-bottom: 20px;
        }

        #cvip-config-schedule-metabox .content-hour-cvip{
            text-align: center;
        }

        #cvip-config-schedule-metabox button{
            margin: auto;
            display: block;
            margin-top: 10px;
        }

        #cvip-config-schedule-metabox .event-content{
            border: 1px #DBDBDB solid;
            text-align: center;
            padding: 5px;
            background: floralwhite;
            position: relative;
        }

        #cvip-config-schedule-metabox td{
            vertical-align: top;
        }

        #cvip-config-schedule-metabox .btn-remove-event{
            position: absolute;
            right: 5px;
            top: 2px;
            cursor: pointer;
        }
    </style>
    <?php
}

function cvip_config_schedule_script() {
    ?>
    <script>
        jQuery(document).ready(function () {

            jQuery(".cvip-schedule-add-event").click(function () {
                var data_id = parseInt(jQuery(this).attr("data-id"));
                var data_day = parseInt(jQuery(this).attr("data-day"));
                jQuery(this).before("<div class='event-content event-content-" + data_day + "' id='event-content-" + data_day + "-" + data_id + "'><span class='btn-remove-event' data-id='" + data_id + "' data-day='" + data_day + "'>X</span>" + ((jQuery("#hour-cvip").html()).replace("hour", "hour-" + data_day + "-" + data_id)).replace("minutes", "minutes-" + data_day + "-" + data_id) + "<input type='text' name='sche-title-" + data_day + "-" + data_id + "' placeholder='Titulo'><input type='text' name='sche-info-" + data_day + "-" + data_id + "' placeholder='Información adicional'></div>");
                jQuery(this).attr("data-id", data_id + 1);
                jQuery("#cvip-config-schedule-metabox .btn-remove-event").click(function () {
                    (jQuery(this).parent()).remove();
                });
            });

            jQuery("#cvip-config-schedule-metabox .btn-remove-event").click(function () {
                (jQuery(this).parent()).remove();
            });

        });

    </script>
    <?php
}

add_action('save_post', 'cvip_config_schedule_save');

function cvip_config_schedule_save($post_id) {

    $myvals = get_post_meta($post_id);

    foreach ($myvals as $key => $val) {
        if (strpos($key, "sche-") !== false) {
            delete_post_meta($post_id, $key);
        }
    }

    foreach ($_POST as $index => $value) {
        if (strpos($index, "sche-") !== false)
            update_post_meta($post_id, $index, esc_attr($value));
    }
}
