jQuery( document ).ready(function() {
  jQuery('[data-countdown]').each(function() {
    var $this = jQuery(this), finalDate = jQuery(this).data('countdown');
    $this.countdown(finalDate, function(event) {
      $this.html(event.strftime('<span class=\"cvip-time-countdown\">%D</span><span class=\"cvip-time-countdown\">%H</span><span class=\"cvip-time-countdown\">%M</span><span class=\"cvip-time-countdown\">%S</span>'));
    });
  });

});
