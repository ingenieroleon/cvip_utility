<?php
/*
  Widget Name: CVIP Countdown
  Description: Cuenta hasta fecha. Al activar se puede usar un simple HTML <span data-countdown="YYYY/MM/DD"></span>
  Author: CVIP
  Author URI:
 */

// Creating the widget
class CVIP_CountDownWidget extends SiteOrigin_Widget {
  function __construct() {
    //Here you can do any preparation required before calling the parent constructor, such as including additional files or initializing variables.
    wp_enqueue_script('countdownminjs',plugins_url('jquery.countdown.min.js', __FILE__ ),array(),false,true);
    wp_enqueue_script('countdownjs',plugins_url('countdown.js', __FILE__ ),array(),false,true);
    //Call the parent constructor with the required arguments.
    parent::__construct(
        // The unique id for your widget.
        'cvip-countdown',
        // The name of the widget for display purposes.
        __('CVIP countdown'),

        // The $widget_options array, which is passed through to WP_Widget.
        // It has a couple of extras like the optional help URL, which should link to your sites help or support page.
        array(
            'description' => __('Cuenta regresiva a fecha.')
        ),
        //The $control_options array, which is passed through to WP_Widget
        array(
        ),

        //The $form_options array, which describes the form fields used to configure SiteOrigin widgets. We'll explain these in more detail later.
        array(
          'fecha' => array(
              'type' => 'text',
              'label' => __('Pon la fecha AAAA/MM/DD'),
              'default' => '2100/01/01'
          ),
        ),

        //The $base_folder path string.
        plugin_dir_path(__FILE__)
    );
  }
  function get_template_name($instance) {
      return "widget";
  }

  function get_template_dir($instance) {
      return "views";
  }

  function get_style_name($instance) {
      return false;
  }

}

siteorigin_widget_register('cvip-countdown', __FILE__, 'CVIP_CountDownWidget');
