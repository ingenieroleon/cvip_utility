<?php
// Report all errors
error_reporting(E_ALL);
$imagen1 = $instance['image1'];
$imagen1_fallback = $instance['image1_fallback'];

$imagen2 = $instance['image2'];
$imagen2_fallback = $instance['image2_fallback'];

$widget_id =  $instance['_sow_form_id'];

$splitwidth =  $instance['splitwidth'];

if ($imagen1) {
  $imagen_antes = wp_get_attachment_url($imagen1);
}elseif($imagen1_fallback) {
  $imagen_antes = $imagen1_fallback;
}else {
  $imagen_antes = plugins_url('../dummy.jpg', __FILE__ );
}


if ($imagen2) {
  $imagen_despues = wp_get_attachment_url($imagen2);
}elseif($imagen2_fallback) {
  $imagen_despues = $imagen2_fallback;
}else {
  $imagen_despues = plugins_url('../dummy.jpg', __FILE__ );
}

?>
				<div id="ad_<?php echo $widget_id ?>" class='twentytwenty-container' style="width: <?=$splitwidth ?>px;margin-left: auto;margin-right: auto;">
  				 <!-- The before image is first -->
  				 <img src="<?php echo $imagen_antes; ?>" />
  				 <!-- The after image is last -->
  				 <img src="<?php echo $imagen_despues; ?>" />
				</div>


        <script type="text/javascript">
        jQuery(document).ready(function() {

          jQuery("#ad_<?php echo $widget_id  ?>").twentytwenty();

        });
        </script>
