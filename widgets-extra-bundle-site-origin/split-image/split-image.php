<?php
/*
  Widget Name: CVIP Split Image
  Description: Creates a Interactive split image.
  Version: 1.20170530
  Author: Ingeniero Leon
  Author URI: http://ingenieroleon.com
 */

 if (!class_exists("SiteOrigin_Widget"))
     return;

 class CVIP_SplitImage extends SiteOrigin_Widget {

     function __construct() {

         $info = array('description' => __('Creates a Interactive split image.'));
         $form_options = array(
             'image1' => array(
                 'type' => 'media',
                 'label' => __( 'Choose a left image', 'widget-form-fields-text-domain' ),
                 'choose' => __( 'Choose left image', 'widget-form-fields-text-domain' ),
                 'update' => __( 'Set left image', 'widget-form-fields-text-domain' ),
                 'library' => 'image',
                 'fallback' => true
             ),
             'image2' => array(
                 'type' => 'media',
                 'label' => __( 'Choose a right image', 'widget-form-fields-text-domain' ),
                 'choose' => __( 'Choose right image', 'widget-form-fields-text-domain' ),
                 'update' => __( 'Set right image', 'widget-form-fields-text-domain' ),
                 'library' => 'image',
                 'fallback' => true
             ),
             'splitwidth' => array(
                  'type' => 'number',
                  'label' => __( 'Enter a number of pixels for width (without px)', 'widget-form-fields-text-domain' ),
                  'default' => '1024',
                  'description' => "PLEASE MAKE SURE TO SET TWO IMAGES WITH EXACTLY SAME HEIGHT",
              ),

         );


         parent::__construct(
           // The unique id for your widget.
           'cvip-split-image',
           // The name of the widget for display purposes.
           __('CVIP Split Image'),
           // The $widget_options array, which is passed through to WP_Widget.
           $info,
           //The $control_options array, which is passed through to WP_Widget
           array(),
           //The $form_options array, which describes the form fields used to configure SiteOrigin widgets. We'll explain these in more detail later.
           $form_options,
           //The $base_folder path string.
           plugin_dir_path(__FILE__)
         );
     }

     function get_template_name($instance) {
         return "widget";
     }

     function get_template_dir($instance) {
         return "views";
     }

     function get_style_name($instance) {
         return false;
     }

     function initialize() {
       $this->register_frontend_scripts(
               array(
                   array( 'twentytwenty',plugins_url('twentytwenty.js', __FILE__ ),array(),false,true ),
                   array( 'jquery.event.move',plugins_url('jquery.event.move.js', __FILE__ ),array(),false,true ),
               )
           );

       $this->register_frontend_styles(
           array(
               array( 'twentytwenty', plugins_url('twentytwenty.css', __FILE__ ) )
           )
       );

     }


 }

 if (!function_exists("siteorigin_widget_register"))
     return;

 siteorigin_widget_register('cvip-split-image', __FILE__, 'CVIP_SplitImage');
