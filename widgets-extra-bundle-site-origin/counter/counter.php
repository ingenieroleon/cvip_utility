<?php
/*
  Widget Name: CVIP Counter
  Description: Contador invertido. Al activar se puede usar un simple HTML <span class="cuentame">###</span>
  Author: CVIP
  Author URI:
 */

// Creating the widget
class CVIP_CounterWidget extends SiteOrigin_Widget {
  function __construct() {
    //Here you can do any preparation required before calling the parent constructor, such as including additional files or initializing variables.
    wp_enqueue_script('counterjs',plugins_url('counter.js', __FILE__ ),array(),false,true);
    wp_enqueue_script('esvisiblejs',plugins_url('esvisible.js', __FILE__ ),array(),false,true);//verificar si es visible
    //Call the parent constructor with the required arguments.
    parent::__construct(
        // The unique id for your widget.
        'cvip-counter',
        // The name of the widget for display purposes.
        __('CVIP counter'),

        // The $widget_options array, which is passed through to WP_Widget.
        // It has a couple of extras like the optional help URL, which should link to your sites help or support page.
        array(
            'description' => __('Contador en aumento.')
        ),
        //The $control_options array, which is passed through to WP_Widget
        array(
        ),

        //The $form_options array, which describes the form fields used to configure SiteOrigin widgets. We'll explain these in more detail later.
        array(
            'numero' => array(
                'type' => 'text',
                'label' => __('Aquí va el numero'),
                'default' => '123'
            ),
        ),

        //The $base_folder path string.
        plugin_dir_path(__FILE__)
    );
}

    function get_template_name($instance) {
        return "widget";
    }

    function get_template_dir($instance) {
        return "views";
    }

    function get_style_name($instance) {
        return false;
    }
}

siteorigin_widget_register('cvip-counter', __FILE__, 'CVIP_CounterWidget');
