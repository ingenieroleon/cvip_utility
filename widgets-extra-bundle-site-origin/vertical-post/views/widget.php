<?php
// Report all errors
// error_reporting(E_ALL);
$post_selector_pseudo_query = $instance['some_posts'];
$post_html = $instance['some_text'];
// Process the post selector pseudo query.
$processed_query = siteorigin_widget_post_selector_process_query($post_selector_pseudo_query);

// Use the processed post selector query to find posts.
$query_result = new WP_Query($processed_query);

// Loop through the posts and do something with them.



if ($query_result->have_posts()) :
  while ($query_result->have_posts()) : $query_result->the_post();
      $post_html_meta=$post_html;
      $META_ATTRIBUTES = get_metadata( get_post_type(), get_the_ID(), '', true );
      $tags_buscar = array("%IMAGE%","%LINK%","%TITLE%","%CONTENIDO%","%AUTHOR%","%DATE%","%COMMENTS%");
      $tags_reemplazar = array(
        wp_get_attachment_url(get_post_thumbnail_id(get_the_ID())),
        get_permalink(),
        get_the_title(),
        strip_tags(substr(get_the_content(), 0, 250)),
        get_the_author(),
        get_the_date(),
        get_comments_number(get_the_ID())
      );
      //REEMPLAZO METAS
      foreach ($META_ATTRIBUTES as $key => $value) {
        $post_html_meta=str_replace("%$key%", $value[0], $post_html_meta);

      }
      echo "$keyvaluelog";
      //MUESTO EL POST CON LOS TAGS REEMPLAZADOS
      echo str_replace($tags_buscar, $tags_reemplazar, $post_html_meta);
  endwhile;
  wp_reset_postdata();
endif; ?>
