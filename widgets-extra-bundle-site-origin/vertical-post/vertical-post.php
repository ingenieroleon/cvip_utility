<?php
/*
  Widget Name: CVIP Post View
  Description: Creates a view for a post selection.
  Version: 3.20170530
  Author: Ingeniero Leon
  Author URI: http://ingenieroleon.com
 */

if (!class_exists("SiteOrigin_Widget"))
    return;

class CVIP_VerticalPostWidget extends SiteOrigin_Widget {

    function __construct() {

        $info = array('description' => __('Despliega un listado de Post en forma vertical.'));

        ob_start();
        ?>
        <div class="externo">
          <div class="imgnoticia"><img src="%IMAGE%" /></div>
          <div class="noticia"><a href="%LINK%"><h3>%TITLE%</h3></a>
              <p>%CONTENIDO%</p>
              <h4 class="administrador">%AUTHOR%</h4>
              <h4 class="calendario">%DATE%</h4>
              <h4 class="comentarios">%COMMENTS% comments</h4>
          </div>
        </div>
        <?php
        $defaulthtml=  ob_get_clean();



        $option_query = array(
            'type' => 'posts',
            'label' => __('Select Posts'),
        );
        $option_html=array(
            'type' => 'textarea',
            'label' => __("Insert post html"
            , 'siteorigin-widgets'),
            'default' => $defaulthtml,
            'rows' => 10,
            'description' => "
            TAGS EXPAND TO POST INFO:
            <span class='etiqueta'>%IMAGE%</span>
            <span class='etiqueta'>%LINK%</span>
            <span class='etiqueta'>%TITLE%</span>
            <span class='etiqueta'>%CONTENIDO%</span>
            <span class='etiqueta'>%AUTHOR%</span>
            <span class='etiqueta'>%DATE%</span>
            <span class='etiqueta'>%COMMENTS%</span>
            ",
          );

        parent::__construct(
          // The unique id for your widget.
          'cvip-vertical-post',
          // The name of the widget for display purposes.
          __('CVIP Post View'),
          // The $widget_options array, which is passed through to WP_Widget.
          $info,
          //The $control_options array, which is passed through to WP_Widget
          array(),
          //The $form_options array, which describes the form fields used to configure SiteOrigin widgets. We'll explain these in more detail later.
          array(
            "some_posts" => $option_query,
            'some_text' => $option_html,
          ),
          //The $base_folder path string.
          plugin_dir_path(__FILE__)
        );
    }

    function get_template_name($instance) {
        return "widget";
    }

    function get_template_dir($instance) {
        return "views";
    }

    function get_style_name($instance) {
        return false;
    }

    function initialize() {
      //INCLUYO JQUERY EN EL ADMIN FORM
      if (!function_exists('admin_scripts')){
        function admin_scripts( $instances ){
          wp_enqueue_script('spanselect',plugins_url('spanselect.js', __FILE__ ),array(),false,true);
            //wp_enqueue_style( ... );
        }
        add_action('siteorigin_widgets_enqueue_admin_scripts_cvip-vertical-post', 'admin_scripts');
      }
    }


}

if (!function_exists("siteorigin_widget_register"))
    return;

siteorigin_widget_register('cvip-vertical-post', __FILE__, 'CVIP_VerticalPostWidget');
